Team members:
Khoa Nguyen - PID A09290499
Oscar Rangel - PID A09632395

Task 1:
 For this task, we use semaphore to manipulate the joining process. First we check if the current thread is parent. Then, we check the status of the child thread whether of its status is Finished. If not, we use semaphore to put the current thread to sleep and wait for the child to finish. When the child finished, the semaphore is signaled to continue the join instructions. If the thread is finished, we allow the project to continue.

In this task, we took the approach of pair programming to make the changes to the code and test the code. The main form of testing for this task came from testing the other tasks 

Task 2:
For this task, in order to implement condition2, we use Lock and follow the structure that is used semaphore. For sleep(), we check if the lock is held by the current thread, then add it to the Wait Queue.The method then release the associated lock and go to sleep.  This part we perform atomically: disable interrupt, call KThread.sleep(), then enable interrupt. Finally we acquire the lock. 

For wake(),  we simply check if the Wait Queue is empty. If not, it goes into atomic section (disabling/enabling interrupt). Inside this section, it starts the first thread in the Wait Queue by setting this thread ready so that the scheduler can start the thread when ready.

In this task, Oscar took care of the first part of coding. we did pair programming (lead by khoa) for debugging. Khoa then led testing.


Task 3:
For this task, we implement the waitUntil method by adding the appropriate number of ticks to the Machine current time, stored in a wakeTime variable. Then it goes into atomic section, it adds the current thread to be into a linkedlist alarmList of threads to wake up, and add the wakeTime to a linkedlist of timeExpiredList. Then it put the current thread to sleep. There is a timeInterrupt() handler that is called by the method that does the context-switch when a thread supposed to wake up.In this handler, the alarms are checked if they are ready to change the thread to ready and does so if necesarry.

In this task, Oscar took care of the first part of coding. we did pair programming (lead by khoa) for debugging. Khoa then led testing.

Task 4:
For this task, we implement two methods: speak() and listen() using the condition variable. For speak(), we first acquire the lock,  then increment number of speakers. It goes into a while loop checking for valid message and number listener greater than 0 (there is at least 1 listen available); else, speaker keep sleeping.  Once a listener is found, we then transfer the word to the message, and decrement number of listeners (since this listener is now paired with this speaker).

For listen(), we implement in very similar way but in reverse fashion. It acquires the lock, increment number of listener. It do a while loop to check if there is a valid message, else the listener keeps sleeping. Once there is a speaker, its set the message back to null (set up for another pair of speaker-listen if there is), decrement number of speaker. Finally it release the lock and returns the message value before set to null.

In this task, Oscar took care of the first part of coding. we did pair programming (lead by Oscar) for debugging. Khoa then led testing.
