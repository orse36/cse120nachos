package nachos.threads;

import java.util.LinkedList;

import nachos.machine.*;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		message = null;
		numberListener = 0;
		numberSpeaker = 0;
		communicatorLock = new Lock();
		speaker = new Condition2(communicatorLock);
		listener = new Condition2(communicatorLock);
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 *
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 *
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {

		communicatorLock.acquire();
		numberSpeaker++;

		// wait for message
		while (numberListener == 0 || message != null) {
			speaker.sleep();
			listener.wake();
		}

		// found a listener
		numberSpeaker--;
		message = new Integer(word);
		listener.wake();

		communicatorLock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 *
	 * @return the integer transferred.
	 */
	public int listen() {

		communicatorLock.acquire();
		numberListener++;

		// get message and loop to try to get new speaker if no message
		while ( message == null) {
			speaker.wake();
			listener.sleep();
		}
		// finally a message so it is received
		int gotMessage = message.intValue();
		message = null;
		numberListener--;
		speaker.wake();

		communicatorLock.release();

		return gotMessage;
	}

	public static void selfTest() {

		// TA Test
		System.out.println("\nTEST COMMUNICATOR 1 - Start [Basic Test]");
		final Communicator com = new Communicator();
		final long times[] = new long[4];
		final int words[] = new int[2];
		KThread speaker1 = new KThread(new Runnable() {
			public void run() {
				com.speak(4);
				times[0] = Machine.timer().getTime();
			}
		});
		speaker1.setName("S1");

		KThread speaker2 = new KThread(new Runnable() {
			public void run() {
				com.speak(7);
				times[1] = Machine.timer().getTime();
			}
		});
		speaker2.setName("S2");

		KThread listener1 = new KThread(new Runnable() {
			public void run() {
				words[0] = com.listen();
				times[2] = Machine.timer().getTime();
			}
		});
		listener1.setName("L1");

		KThread listener2 = new KThread(new Runnable() {
			public void run() {
				words[1] = com.listen();
				times[3] = Machine.timer().getTime();
			}
		});
		listener2.setName("L2");

		speaker1.fork();
		speaker2.fork();
		listener1.fork();
		listener2.fork();
		speaker1.join();
		speaker2.join();
		listener1.join();
		listener2.join();
		
		System.out.println("First Message:" + words[0] + "\nSecond Message: " + words[1]);	
		Lib.assertTrue(words[0] == 4, "Didn't listen back spoken word.");
		Lib.assertTrue(words[1] == 7, "Didn't listen back spoken word.");
		Lib.assertTrue(times[0] < times[2], "speak returned before listen.");
		Lib.assertTrue(times[1] < times[3], "speak returned before listen.");
		System.out.println("TEST COMMUNICATOR 1 - Done");
		
		//////////////////////////////////////////////////
		
		System.out.println("\nTEST COMMUNICATOR 2 - Start [One listener, One speaker]");

		final Communicator com2 = new Communicator();
		final int msg[] = new int [1];
		KThread testSpeaker = new KThread(new Runnable() {
			public void run() {
				com2.speak(9);
			}
		});
		testSpeaker.setName("testSpeaker");
		
		KThread testListener = new KThread(new Runnable() {
			public void run() {
				msg[0] = com2.listen();
			}
		});
		testListener.setName("testListener");
		
		testListener.fork();
		testSpeaker.fork();
		testListener.join();
		testSpeaker.join();
		
		System.out.println("Message (should be 9): " + msg[0]);
		
		System.out.println("TEST COMMUNICATOR 2 - Done");

	}

	private Integer message;
	private int numberListener;
	private int numberSpeaker;
	private Lock communicatorLock;
	private Condition2 listener;
	private Condition2 speaker;
}
