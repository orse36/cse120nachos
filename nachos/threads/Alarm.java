package nachos.threads;

import java.util.LinkedList;

import nachos.machine.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
    /**
     * Allocate a new Alarm. Set the machine's timer interrupt handler to this
     * alarm's callback.
     *
     * <p><b>Note</b>: Nachos will not function correctly with more than one
     * alarm.
     */
    public Alarm() {
	Machine.timer().setInterruptHandler(new Runnable() {
		public void run() { timerInterrupt(); }
	    });
	
	alarmList = new LinkedList<KThread>();
	timeExpireList = new LinkedList<Long>();
	
    }

    /**
     * The timer interrupt handler. This is called by the machine's timer
     * periodically (approximately every 500 clock ticks). Causes the current
     * thread to yield, forcing a context switch if there is another thread
     * that should be run.
     */
    public void timerInterrupt() {
    	
    	/*for( int i = alarmList.size(); i > 0; i--){
    		if (Machine.timer().getTime() >= timeExpireList.get(i-1))
    		{
    			alarmList.get(i-1).ready();
    			alarmList.remove(i-1);
    			timeExpireList.remove(i-1);
    		}
    	}*/
    	KThread.currentThread().yield();
	
    }
    


    /**
     * Put the current thread to sleep for at least <i>x</i> ticks,
     * waking it up in the timer interrupt handler. The thread must be
     * woken up (placed in the scheduler ready set) during the first timer
     * interrupt where
     *
     * <p><blockquote>
     * (current time) >= (WaitUntil called time)+(x)
     * </blockquote>
     *
     * @param	x	the minimum number of clock ticks to wait.
     *
     * @see	nachos.machine.Timer#getTime()
     */
    public void waitUntil(long x) {
	// for now, cheat just to get something working (busy waiting is bad)
    	long wakeTime = Machine.timer().getTime() + x;
	
    	//remove previous work
    	while (wakeTime > Machine.timer().getTime())
	    	KThread.yield();
    	 
	
    	//enter "do not interrupt"
	 	//Machine.interrupt().disable();
	 	
	 	//alarmList.add(KThread.currentThread());
	 	//timeExpireList.add(wakeTime);
	 	
	 	//KThread.sleep();
	 	
	 	//Machine.interrupt().enable();
	 	
    }
    
    public static void selfTest() {
    	/*
		long timeStart;
		long timeDone;
		long timeToWait;
		long timeWaited;
    	//TA Test
		System.out.println("\nTEST ALARM 1 - Start [Basic Test]");
    	KThread t1 = new KThread(new Runnable() {
            public void run() {
                long time1 = Machine.timer().getTime();
                int waitTime = 10000;
                System.out.println("Thread calling wait at time:" + time1);
                ThreadedKernel.alarm.waitUntil(waitTime);
                System.out.println("Thread woken up after:" + (Machine.timer().getTime() - time1));
                Lib.assertTrue((Machine.timer().getTime() - time1) > waitTime, " thread woke up too early.");
                
            }
        });
        t1.setName("T1");
        t1.fork();
        t1.join();
		System.out.println("TEST ALARM 1 - Done");
        
		System.out.println("\nTEST ALARM 2 - Start [Loop 5 times & test with 0 wait time]");
	
		Alarm t2 = new Alarm();
		for (int i = 0 ; i < 5 ; i++)
		{	
			timeStart = Machine.timer().getTime();
			timeToWait = (long)((1000)*i);
			System.out.println("Thread going to wait at-- " + timeStart +  " -----for-- " + timeToWait );
			t2.waitUntil(timeToWait);
			timeDone = Machine.timer().getTime();
			timeWaited = timeDone - timeStart;
			System.out.println("Thread wake up at-------- " + timeDone + " --waited-- " + timeWaited);
			Lib.assertTrue(timeWaited > timeToWait, "The thread wakes up before wait time");
		}
		System.out.println("TEST ALARM 2 - Done");
		
		System.out.println("\nTEST ALARM 3 - Start [Neagtive WaitTime]");
		Alarm test = new Alarm();	
			timeStart = Machine.timer().getTime();
			timeToWait = -10;
			System.out.println("Thread going to wait at-- " + timeStart +  " -----for  " + timeToWait );
			test.waitUntil(timeToWait);
			timeDone = Machine.timer().getTime();
			timeWaited = timeDone - timeStart;
			System.out.println("Thread wake up at-------- " + timeDone + " --waited  " + timeWaited);
			Lib.assertTrue(timeWaited > timeToWait, "The thread wakes up before wait time");
		System.out.println("TEST ALARM 3 - Done");*/
    }
    
    LinkedList<KThread> alarmList;
    LinkedList<Long> timeExpireList;
}
