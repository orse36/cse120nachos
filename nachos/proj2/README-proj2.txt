Team Member:
Khoa Nguyen  -- A09290499 
Oscar Rangel -- A0963239

In this project we decided to keep the group of two so we can understand the material batter and prepare ourselves for the final. I (Khoa) did most of the testing, and couple of bug fixes. Oscar did most of the coding for the tasks. Having group of two allow us to help each other out more directly. 

Task 1:
Solution: We implement the file system closely to the suggestion from the project description. We had a switch state that includes all of the argument for the system calls. One of the arguments is syscall which determine which system calls to handle, and the switch statement pass the appropriate argument for the handler. From there, for each handler, we convert the int parameters (a0, a1, a2, a3) into correct type; for instance convert to String (char* name) by using readVirtualMemoryString(). This way, having all of the argument initially as int, keeping parameter not explicit, we can prevent user program passing in bogus arguments. Because we can do simple check for each argument for instance comparing with a1 with 0 for valid size. We have fileArray to keep track of files being created, open, and such. And once the file is closed or deleted, it’s removed from the array. And we set this array size to 16 as the requirement of max 16 files in the system. 
Challenges: We had to first fix join() from previous project 1 where we didn’t get full points. We removed static for semaphore in this case. And from there we got the syscall handlers to work correctly. We also received help from TAs to be able to open .coff files (before making syscalls). 
Testing: We used mostly the tests provided by our TAs. The program runs correctly. We did not have time to do extensive testing on task 1, as we have to set priority to get other tasks completed. 

Task 2:
Solution: In order to do multiprogramming, which requires address translation, we initialized a linkedlist for physical pages to store translated address from virtual to physical. We follow the instruction and start modifying UserProcess.readVirtualMemory and UserProcess.writeVirtualMemory for the system to read and write data in the virtual address space correctly.  For both methods, we use these calculations similar to what we learn in class to translate address as follow:
		int vpn = vaddr / pageSize;
		int voffset = vaddr % pageSize;
		TranslationEntry tEntry = pageTable[vpn];
		tEntry.used = true;
		int paddr = tEntry.ppn*pageSize + voffset;
We are basically mapping the virtual to physical address in these methods. We also modify loadSections() to get number of pages and unloadSections to get rid them when system done running. We follow these instructions as close as we can, but code were breaking at certain points. Thus we had to roll back our git depository to code that compile successfully. 
Challenges: This is the last task that we implement due to difficulty in address translation. The hardest challenge we face is to get the right physical address from virtual address with the page table. We have the most problems with this task, mostly due to difficulty of debugging, tracking down where the breaking point is. Our best option was to use system.out.print statement to find where the system breaking, step by step. 
Testing: We test task 2 together with task 3 as suggested. 

Task 3:
Solution: In order to implement exec/join/exit, we use the structure created from task 1. We add in the switch statement for each of these handler and pass in appropriate parameters. We also do parameter checking for user program as similar to task 1, size and file name. Fro exec, we start the process and check if it executing. The we link child and parents processes. For exit, we go through file array and close each individual file. We also, notify the parent process of the closing file. Lastly for join, we go through each child and verify that they have been close and wait if the child process hasn’t suing semaphore. 
Challenges: It was also difficult to test and we were running out of time. 
Testing: Again, we use the majority of tests provided by TAs. Unfortunately the test doesn’t run correctly as we wanted due to our incompletion for task 2. We could only tell for certain that our handlers for exec works as we can call and execute loopSimple.coff . However our handleJoin doesn’t seem to work correctly as the Thread doesn’t set to finished. 
